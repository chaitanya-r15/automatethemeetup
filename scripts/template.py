import sys
from configparser import ConfigParser

def createNew():
    newTemplate = open('config-template.ini').read()
    open('meetup-template', 'w').write(newTemplate)

def announce():
    parser = ConfigParser()
    parser.read('meetup-template')

    title = parser.get('subject', 'title')

    date = parser.get('meetup details', 'date')
    month = parser.get('meetup details', 'month')
    year = parser.get('meetup details', 'year')
    time = parser.get('meetup details', 'time')

    subject_line = title.format(date, month, year)

    message = parser.get('message', 'name')
    message = message.format(date, month, year, time)

    about_us = parser.get('About us', 'text')

    venue_checklist = parser.get('Venue checklist', 'text')
    venue_checklist = venue_checklist.format(date, month, year, time)

    ajenda = parser.get('Ajenda', 'text')
    reach_us = parser.get('How to reach us', 'text')


    print("Subject: ", subject_line, "\n\n")
    print("message", message)
    print(about_us)
    print(venue_checklist)
    print(ajenda)
    print(reach_us)

    return {"subject": subject_line,
            "message": message+"\n"+about_us+"\n"+
                       venue_checklist+"\n"+ajenda+"\n"+reach_us}

# {0} - Month
# {1} - Date
# {2} - year
# {3} - time

if __name__ == '__main__':
    if len(sys.argv) > 1:
        if sys.argv[1] == 'new':
            createNew()
        elif sys.argv[1] == 'announce':
            announce()
        else:
            print("usage template.py new|announce ")
            # print("usage template.py announce")
